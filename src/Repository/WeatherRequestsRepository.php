<?php

namespace App\Repository;

use App\Entity\WeatherRequests;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WeatherRequests|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeatherRequests|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeatherRequests[]    findAll()
 * @method WeatherRequests[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherRequestsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeatherRequests::class);
    }

    // /**
    //  * @return WeatherRequests[] Returns an array of WeatherRequests objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WeatherRequests
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
