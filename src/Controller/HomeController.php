<?php

// src/Controller/HomeController.php

namespace App\Controller;

use App\Entity\WeatherRequests;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController {
//Lista API z których są pobierane dane o temperaturze
    private $apiNames = [
        'Openweathermap',
        'Weatherstack',
        'Weatherapi'
    ];

    public function index(Request $request) {
        $weatherRequest = new WeatherRequests();
        $form = $this->createFormBuilder($weatherRequest)
                ->add('country', CountryType::class, ['label' => 'Kraj'])
                ->add('city', TextType::class, ['label' => 'Miasto'])
                ->add('send', SubmitType::class, ['label' => 'Sprawdź'])
                ->getForm();

        $form->handleRequest($request);
        $country = false;
        $city = false;
        $temperature = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $rqData = $form->getData();
            $country = $this->getCountry($rqData->getCountry());
            $city = $rqData->getCity();
            $getNew = false;
            $entityManager = $this->getDoctrine()->getManager();
            //Sprawdzam czy było już zapytanie o taką lokalizacje
            $checkOld = $entityManager->getRepository(WeatherRequests::class)->findOneBy(['country' => $rqData->getCountry(), 'city' => mb_strtolower($city)]);
            if (!empty($checkOld)) {
                //Sprawdzam czy minął czas wygaśnięcia danych pobranych już dla tej lokalizacji
                if ($checkOld->getExpireAt()->getTimestamp() < time()) {
                    $getNew = true;
                }
            } else {
                $getNew = true;
            }
            if (!empty($checkOld) && !$getNew) {
                //Jeżeli w bazie są pobrane dane i nie minął czas wygaśnięcia podaje czas z bazy
                $temperature = $checkOld->getTemperature();
                //Aktualizuje ilość wywołań
                        $checkOld->setRequestCount(($checkOld->getRequestCount() + 1));
                        $entityManager->flush();
            } else {
                //Pobieram temperaturę przez API
                $temperature = $this->getTemperature($rqData->getCountry(), $city);
                if (!empty($temperature)) {
                    if (!empty($checkOld)) {
                        //Aktualizuje wpus w bazie;
                        $checkOld->setExpireAt(new \DateTime('+ 2 hours'));
                        $checkOld->setTemperature($temperature);
                        $checkOld->setRequestCount(($checkOld->getRequestCount() + 1));
                        $entityManager->flush();
                    } else {
                        //Dodaję nowy wpis do bazy
                        $weatherRequest->setCountry($rqData->getCountry());
                        $weatherRequest->setCity(mb_strtolower($city));
                        $weatherRequest->setTemperature($temperature);
                        $weatherRequest->setRequestCount(1);
                        $weatherRequest->setExpireAt(new \DateTime('+ 2 hours'));
                        $entityManager->persist($weatherRequest);
                        $entityManager->flush();
                    }
                }
            }
        }

        return $this->render('home/index.html.twig', [
                    'form' => $form->createView(),
                    'country' => $country,
                    'city' => $city,
                    'temperature' => $temperature
        ]);
    }
/**
 * Pobieranie temeratury z listy zdefiniowanych API
 * @param type $country
 * @param type $city
 * @return string
 */
    private function getTemperature($country, $city) {
        $temperatures = [];
        foreach ($this->apiNames as $apiName) {
            $apiFunc = 'getBy' . $apiName;
            $temperature = $this->{$apiFunc}(['country' => $country, 'city' => $city]);
            if (!empty($temperature)) {
                $temperatures[] = $temperature;
            }
        }
        if (!empty($temperatures)) {
            return round(array_sum($temperatures) / count($temperatures));
        } else {
            return 'Brak danych';
        }
    }

    private function getByOpenweathermap($data) {
        $apiKey = '6cee928d8f5d6221515eb0d27dfbd68b';
        $url = 'http://api.openweathermap.org/data/2.5/weather?q=' . urlencode($data['city']) . ',' . $data['country'] . '&units=metric&appid=' . $apiKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        if (!empty($result)) {
            $result = json_decode($result, true);
            if ($result['cod'] !== 200) {
                return false;
            } else {
                return $result['main']['temp'];
            }
        }
        return false;
    }

    private function getByWeatherapi($data) {
        $apiKey = 'fe3bd5db3efb490c862134752201810';
        $url = 'http://api.weatherapi.com/v1/current.json?key='.$apiKey.'&q=' . urlencode($this->trimSpecialChar($data['city'])) . ',' . $data['country'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        if (!empty($result)) {
            $result = json_decode($result, true);
            if (!empty($result['error'])) {
                return false;
            } else {
                return $result['current']['temp_c'];
            }
        }
        return false;
    }

    private function getByWeatherstack($data) {
        $apiKey = '452077bc3828c267571be0f67313089d';
        $url = 'http://api.weatherstack.com/current?query=' . urlencode($this->trimSpecialChar($data['city'])) . ',' . $data['country'] . '&units=m&access_key=' . $apiKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        if (!empty($result)) {
            $result = json_decode($result, true);
            if (!empty($result['error'])) {
                return false;
            } else {
                return $result['current']['temperature'];
            }
        }
        return false;
    }

    private function trimSpecialChar($text) {
        $chars = [
            'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ś' => 's', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z'
        ];
        return str_replace(array_keys($chars), array_values($chars), mb_strtolower($text));
    }

    private function getCountry($country = null) {
        $countries = [
            'AF' => 'Afghanistan',
            'AX' => 'Åland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas (the)',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia (Plurinational State of)',
            'BQ' => 'Bonaire, Sint Eustatius and Saba',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory (the)',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'CV' => 'Cabo Verde',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'KY' => 'Cayman Islands (the)',
            'CF' => 'Central African Republic (the)',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands (the)',
            'CO' => 'Colombia',
            'KM' => 'Comoros (the)',
            'CD' => 'Congo (the Democratic Republic of the)',
            'CG' => 'Congo (the)',
            'CK' => 'Cook Islands (the)',
            'CR' => 'Costa Rica',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CW' => 'Curaçao',
            'CY' => 'Cyprus',
            'CZ' => 'Czechia',
            'CI' => 'Côte d\'Ivoire',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic (the)',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'SZ' => 'Eswatini',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (the) [Malvinas]',
            'FO' => 'Faroe Islands (the)',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories (the)',
            'GA' => 'Gabon',
            'GM' => 'Gambia (the)',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island and McDonald Islands',
            'VA' => 'Holy See (the)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran (Islamic Republic of)',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KP' => 'Korea (the Democratic People\'s Republic of)',
            'KR' => 'Korea (the Republic of)',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic (the)',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands (the)',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia (Federated States of)',
            'MD' => 'Moldova (the Republic of)',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands (the)',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger (the)',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands (the)',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestine, State of',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines (the)',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'MK' => 'Republic of North Macedonia',
            'RO' => 'Romania',
            'RU' => 'Russian Federation (the)',
            'RW' => 'Rwanda',
            'RE' => 'Réunion',
            'BL' => 'Saint Barthélemy',
            'SH' => 'Saint Helena, Ascension and Tristan da Cunha',
            'KN' => 'Saint Kitts and Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin (French part)',
            'PM' => 'Saint Pierre and Miquelon',
            'VC' => 'Saint Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SX' => 'Sint Maarten (Dutch part)',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia and the South Sandwich Islands',
            'SS' => 'South Sudan',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan (the)',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard and Jan Mayen',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan (Province of China)',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania, United Republic of',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands (the)',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates (the)',
            'GB' => 'United Kingdom of Great Britain and Northern Ireland (the)',
            'UM' => 'United States Minor Outlying Islands (the)',
            'US' => 'United States of America (the)',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela (Bolivarian Republic of)',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands (British)',
            'VI' => 'Virgin Islands (U.S.)',
            'WF' => 'Wallis and Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe'
        ];
        if (!empty($country)) {
            if (!empty($countries[$country])) {
                return $countries[$country];
            } else {
                return null;
            }
        }
        return $countries;
    }

}
